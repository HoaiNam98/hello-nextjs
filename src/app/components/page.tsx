/* eslint-disable @next/next/no-img-element */
"use client";
import { useLayoutEffect } from 'react';
import styles from './Mansory.module.scss'

const array: { path: string }[] = Array(6).fill({
    path: 'https://source.unsplash.com/random'
})

const MansoryLayout: React.FC = () => {
    useLayoutEffect(() => {
        console.log(`.${styles['p-mansory_wrapper']}`)
        const imgs = document.querySelectorAll(`.${styles['p-mansory_wrapper']}`);
        console.log("🚀 ~ file: page.tsx:12 ~ useEffect ~ imgs:", imgs)
    }, [])
    return (
        <div className={styles['p-mansory']}>
            {
                array.map((item, idx) => (
                    <div key={`item-${idx.toString()}`} className={styles['p-mansory_wrapper']}>
                        <div className={styles['p-mansory_image']}>
                            <img src={item.path} alt='' />
                        </div>
                    </div>
                ))
            }
        </div>
    )
};

export default MansoryLayout;